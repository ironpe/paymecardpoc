from PIL import Image, ImageDraw, ImageFont
import math

def add_textToCard(ppCard, strPagaA, strURL):
    fontDesc = ImageFont.truetype("FontsFree-Net-AvertaStandard-Regular.ttf", 18)
    fontURL = ImageFont.truetype("FontsFree-Net-AvertaStandard-Regular.ttf", 22)


    drawText = ImageDraw.Draw(ppCard)
    drawText.text((317, 95),strURL,(252,76,2),font=fontURL)
    drawText.text((317, 60),strPagaA,(0,0,0),font=fontDesc)

    return ppCard
    



def add_PublicProfileImToCard(strContainerPath="container.png", strProfilcePicturePath="profilePicture.png", intSize="160"):

    imContainer = Image.open(strContainerPath)
    imProfilePicture = Image.open(strProfilcePicturePath)

    # if the container cant't be open we need to save a default image and log the error to the system
    # validate the format of the images, so if them are in a different format than PNG convert it to PNG
    # validate the min heigh is 160 and also width

    # current size of the image:
    widthProfilePicture, heightProfilePicture = imProfilePicture.size
    # print(width, height)

    minSize = intSize
    # size of the image 160x160
    profilePictureSize = (minSize,minSize)
    # profile position (40,80) left, upper
    profilePicturePosition = (40,80)

    # we need to adjust the profile picture to the right size & ratio
    # center of the circle 100,160

    if(widthProfilePicture == heightProfilePicture):
        imProfilePictureAdjusted = imProfilePicture.resize(profilePictureSize)
    else:
        factor = math.trunc((widthProfilePicture if widthProfilePicture <= heightProfilePicture else heightProfilePicture)/minSize)
        profilePictureSize = (math.trunc(widthProfilePicture/factor), math.trunc(heightProfilePicture/factor))
        imProfilePictureAdjusted = imProfilePicture.resize(profilePictureSize)

    # Circle processing 

    mask_imCircle = Image.new("L", imProfilePictureAdjusted.size, 0)
    drawCircle = ImageDraw.Draw(mask_imCircle)
    drawCircle.ellipse((1, 1, 159, 159), fill=255)

    back_imContainer = imContainer.copy()
    back_imContainer.paste(imProfilePictureAdjusted, profilePicturePosition, mask_imCircle)
    return back_imContainer
    # back_imContainer.save('paymeURL_ProfilePicture_MEname.png', quality=95)

CustomImContainer = add_PublicProfileImToCard("container_noText.png","profilePictureAP.png",160)
CustomImContainer = add_textToCard(CustomImContainer, "Paga a Alfredo Peña", "clip.mx/@alfredopenha")
CustomImContainer.save('paymeURL_ProfilePicture_MEname.png', quality=95)